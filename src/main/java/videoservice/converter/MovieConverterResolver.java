package videoservice.converter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Component
@RequiredArgsConstructor
@Slf4j
public class MovieConverterResolver {
    @Named("mapPoster")
    public byte[] mapPoster(byte[] poster) {
        return poster;
    }
}
