package videoservice.converter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import videoservice.dto.MovieLoading;
import videoservice.entity.Movie;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        uses = {MovieConverterResolver.class})
public interface MovieConverter {
    @Mapping(target = "originalName", source = "title")
    @Mapping(target = "poster", source = "movieLoading.poster", qualifiedByName = "mapPoster")
    Movie convertRequestVideoToEntity(MovieLoading movieLoading);
}
