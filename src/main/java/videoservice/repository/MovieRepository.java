package videoservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import videoservice.entity.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer>, QuerydslPredicateExecutor<Movie> {
}
