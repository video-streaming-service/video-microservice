package videoservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import videoservice.entity.MovieLoadHistory;

@Repository
public interface MovieLoadingHistoryRepository extends JpaRepository<MovieLoadHistory, Integer> {
//todo: сделать сохранение history
}
