package videoservice.ffmpeg;

import lombok.RequiredArgsConstructor;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@RequiredArgsConstructor
public class HlsConverter {
    private final FFmpeg ffmpeg;

    public void convertToHLS(String inputFile, String output360pDir, String output720pDir, String output1080pDir, String parentCatalog) throws IOException {
        FFmpegExecutor executor = new FFmpegExecutor(ffmpeg);
        Files.createDirectories(Paths.get(output360pDir));
        Files.createDirectories(Paths.get(output720pDir));
        Files.createDirectories(Paths.get(output1080pDir));

        FFmpegBuilder[] builders = new FFmpegBuilder[]{
                createHLSBuilder(inputFile, output1080pDir, "1920x1080", "5M", "128k"),
                createHLSBuilder(inputFile, output720pDir, "1280x720", "3M", "96k"),
                createHLSBuilder(inputFile, output360pDir, "640x360", "1M", "64k")
        };

        CountDownLatch latch = new CountDownLatch(3);
        try (ExecutorService executorService = Executors.newFixedThreadPool(3)) {
            for (var builder : builders) {
                executorService.submit(() -> runJob(executor, builder, latch));
            }
            latch.await();
            writeMasterPlaylist(parentCatalog);
        } catch (InterruptedException e) {
            throw new RuntimeException("Ошибка ожидания завершения HLS-задач", e);
        }
    }

    private FFmpegBuilder createHLSBuilder(String inputFile, String outputDir, String resolution, String videoBitrate, String audioBitrate) {
        return new FFmpegBuilder()
                .setInput(inputFile)
                .addOutput(outputDir + "/output.m3u8")
                .setVideoCodec("libx264")
                .setAudioCodec("aac")
                .setFormat("hls")
                .addExtraArgs("-x264-params", "nal-hrd=cbr:force-cfr=1")
                .addExtraArgs("-b:v", videoBitrate)
                .addExtraArgs("-maxrate", videoBitrate)
                .addExtraArgs("-minrate", videoBitrate)
                .addExtraArgs("-bufsize", Integer.parseInt(videoBitrate.replace("M", "")) * 2 + "M")
                .addExtraArgs("-r", "24", "-g", "120", "-sc_threshold", "0", "-keyint_min", "120")
                .addExtraArgs("-hls_time", "5", "-hls_playlist_type", "vod")
                .addExtraArgs("-hls_flags", "independent_segments")
                .addExtraArgs("-hls_segment_filename", outputDir + "/data%02d.ts")
                .addExtraArgs("-map", "0:v", "-s:v", resolution)
                .addExtraArgs("-map", "0:a", "-b:a", audioBitrate)
                .done();
    }

    private void runJob(FFmpegExecutor executor, FFmpegBuilder builder, CountDownLatch latch) {
        try {
            executor.createJob(builder).run();
        } finally {
            latch.countDown();
        }
    }

    private void writeMasterPlaylist(String parentCatalog) throws IOException {
        String masterPlaylistContent = """
                #EXTM3U
                               #EXT-X-STREAM-INF:BANDWIDTH=1000000,RESOLUTION=640x360
                               output_360p/output.m3u8
                               #EXT-X-STREAM-INF:BANDWIDTH=3000000,RESOLUTION=1280x720
                               output_720p/output.m3u8
                               #EXT-X-STREAM-INF:BANDWIDTH=5000000,RESOLUTION=1920x1080
                               output_1080p/output.m3u8
                """;

        Files.write(Paths.get(parentCatalog, "master.m3u8"), masterPlaylistContent.getBytes());
    }

}
