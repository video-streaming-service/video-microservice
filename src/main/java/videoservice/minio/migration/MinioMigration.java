package videoservice.minio.migration;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


/**
 * Класс для создания миграции в S3 хранилище
 */
@Component
@RequiredArgsConstructor
public class MinioMigration implements CommandLineRunner {
    private final MinioClient minioClient;
    // Имя бакета, в котором будет храниться видео
    private static String VIDEO_СONTENT_BUCKET_NAME = "video-content";
    //Имя бакета, в которо хранится обложка
    private static String VIDEO_ENVELOPE_BUCKET_NAME = "video-envelope";

    @Override
    public void run(String... args) throws Exception {
        createIfNotExist(VIDEO_СONTENT_BUCKET_NAME);
        createIfNotExist(VIDEO_ENVELOPE_BUCKET_NAME);
    }

    /**
     * Создание бакетов, если не были созданы.
     *
     * @param bucketName имя бакета, который будем создавать
     * @throws Exception
     */
    private void createIfNotExist(String bucketName) throws Exception {
        if (!minioClient.bucketExists(BucketExistsArgs
                .builder()
                .bucket(bucketName)
                .build())) {
            MakeBucketArgs mbArgs = MakeBucketArgs.builder()
                    .bucket(bucketName).build();
            minioClient.makeBucket(mbArgs);
        }
    }
}
