package videoservice.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Data
public class MovieDescription {
    private UUID id;
    private MultipartFile poster;
    private String description;
    private String originalName;
    private String genre;
}
