package videoservice.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.multipart.MultipartFile;
import videoservice.entity.enums.GenreEnum;

/**
 * Dto, которую принимаем с фронтенда представления о фильме для сохранения
 */
@Data
@RequiredArgsConstructor
public class MovieLoading {
    String title;
    byte[] poster;
    String description;
    GenreEnum genre;
}
