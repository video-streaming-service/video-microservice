package videoservice.dto;

import lombok.Data;
import videoservice.projection.MovieProjection;

import java.util.UUID;

@Data
public class MovieProjectionDto {
    private UUID id;
    private String description;
    private String originalName;
    private String genre;
    private String poster;

    public MovieProjectionDto(MovieProjection movieProjection) {
        id = movieProjection.getId();
        description = movieProjection.getDescription();
        originalName = movieProjection.getOriginalName();
        genre = movieProjection.getGenre();
        poster = movieProjection.getBase64Poster();
    }
}
