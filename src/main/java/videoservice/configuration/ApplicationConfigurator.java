package videoservice.configuration;

import io.minio.MinioClient;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import net.bramp.ffmpeg.FFmpeg;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;


@Configuration
public class ApplicationConfigurator {
    @Value("${minio.endpoint}")
    private String endpoint;
    @Value("${minio.accessKey}")
    private String accessKey;
    @Value("${minio.secretKey}")
    private String secretKey;

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(endpoint)
                .credentials(accessKey, secretKey)
                .build();
    }

    @Bean
    public FFmpeg ffmpeg() {
        try {
            return new FFmpeg("C:\\ffmpeg-7.1-essentials_build\\bin\\ffmpeg.exe");
        } catch (IOException e) {
            throw new RuntimeException("Yпс " + e.getMessage());
        }
    }
}
