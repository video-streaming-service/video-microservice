package videoservice.filter;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Component;
import videoservice.entity.QMovie;
import videoservice.entity.QMovieLoadHistory;
import videoservice.entity.enums.GenreEnum;
import videoservice.entity.enums.MovieStatusEnum;
import videoservice.repository.MovieLoadingHistoryRepository;

@Component
public class LibraryFilter implements DslMapper<MovieFilter> {
    @Override
    public Predicate createFilter(MovieFilter movieFilter) {
        QMovieLoadHistory qMovieLoadHistory = QMovieLoadHistory.movieLoadHistory;
        return new JPAQuery<Void>()
                .from(QMovie.movie)
                .join(qMovieLoadHistory)
                .on(QMovie.movie.id.eq(qMovieLoadHistory.movie.id)
                        .and(qMovieLoadHistory.loadingStatus.eq(MovieStatusEnum.FINISH)))
                .where(startWithName(movieFilter.getTitle())
                        .and(eqGenre(movieFilter.getGenre())))
                .select(QMovie.movie.id)
                .exists();
    }



    private BooleanExpression startWithName(String name) {
        return name == null ?
                Expressions.asBoolean(true).isTrue() :
                QMovie.movie.originalName.startsWith(name);
    }

    private BooleanExpression eqGenre(GenreEnum genre) {
        return genre == null ?
                Expressions.asBoolean(true).isTrue() :
                QMovie.movie.genre.eq(genre);
    }
}
