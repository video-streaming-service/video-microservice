package videoservice.filter;


import com.querydsl.core.types.Predicate;

public interface DslMapper <Filter>{
 Predicate createFilter(Filter filter);
}
