package videoservice.filter;

import lombok.Data;
import videoservice.entity.enums.GenreEnum;

@Data
public class MovieFilter {
    private String title;
    private GenreEnum genre;
}
