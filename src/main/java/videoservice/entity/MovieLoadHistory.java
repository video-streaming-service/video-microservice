package videoservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import videoservice.entity.enums.MovieStatusEnum;

import java.time.Instant;

@Entity
@Table(name = "movie_load_history")
@Getter
@Setter
public class MovieLoadHistory {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @CreationTimestamp
    private Instant creationDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "movie_id")
    @JsonIgnore
    private Movie movie;

    @Enumerated(EnumType.STRING)
    private MovieStatusEnum loadingStatus = MovieStatusEnum.PROCESSING;

    @Column(name = "message")
    private String message;
}
