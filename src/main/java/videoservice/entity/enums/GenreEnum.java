package videoservice.entity.enums;

import lombok.Getter;

@Getter
public enum GenreEnum {
    COMEDY("Комедия"),
    THRILLER("Триллер");

    GenreEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    private final String name;
}
