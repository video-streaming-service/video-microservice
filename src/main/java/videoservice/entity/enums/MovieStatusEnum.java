package videoservice.entity.enums;

import lombok.Getter;

@Getter
public enum MovieStatusEnum {
    PROCESSING("Загружается"),
    FINISH("Загружено"),
    ERROR("Ошибка");
    private final String typeStatus;

    MovieStatusEnum(String name) {
        typeStatus = name;
    }

    @Override
    public String toString() {
        return typeStatus;
    }
}
