package videoservice.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import videoservice.entity.enums.GenreEnum;

import java.util.UUID;

@Entity
@Table(name = "movie")
@Getter
@Setter
public class Movie {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column
    private String description;

    @Column(name = "original_name")
    private String originalName;

    @Enumerated(EnumType.STRING)
    private GenreEnum genre;

    @Column(name = "poster")
    private byte[] poster;
}
