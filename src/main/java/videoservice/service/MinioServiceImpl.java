package videoservice.service;

import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@Service
@RequiredArgsConstructor
public class MinioServiceImpl implements S3Service {
    private final MinioClient minioService;

    @Override
    public InputStream getObjectByLabelAndBucketName(String objectName, String bucket) {
        var getObject = GetObjectArgs.builder()
                .object(objectName)
                .bucket(bucket).build();
        try {
            return minioService.getObject(getObject);
        } catch (Exception e) {
            throw new RuntimeException("Не удалось найти файл %s по причине %s".formatted(objectName, e.getMessage()));
        }
    }

    @Override
    public void saveObject(File object, String bucket,String name) {
        try (var fileStream = new FileInputStream(object)) {
            minioService.putObject(PutObjectArgs
                    .builder()
                    .bucket(bucket)
                    .object(name)
                    .stream(fileStream, object.length(), -1L).build());
        } catch (Exception e) {
            throw new RuntimeException("Не удалось сохранить файл %s по причине %s"
                    .formatted(object.getName(), e.getMessage()));
        }
    }

}
