package videoservice.service;

import java.io.File;
import java.io.InputStream;

/**
 * Контракт для взаимодействия с хранилещем
 */

public interface S3Service {
    InputStream getObjectByLabelAndBucketName(String label, String bucket);

    void saveObject(File object, String bucket, String label);
}
