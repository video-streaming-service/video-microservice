package videoservice.service;

import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.core.io.Resource;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import videoservice.dto.MovieProjectionDto;
import videoservice.entity.Movie;
import videoservice.filter.DslMapper;
import videoservice.filter.MovieFilter;
import videoservice.projection.MovieProjection;
import videoservice.repository.MovieLoadingHistoryRepository;
import videoservice.repository.MovieRepository;


import java.io.File;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class VideoService {
    private final S3Service storageService;
    private static final String VIDEO_BUCKET = "video-content";
    private final MovieLoadingHistoryRepository movieLoadingHistoryRepository;
    private final MovieRepository movieRepository;
    private final DslMapper<MovieFilter> filterDslMapper;

    public Resource getResource(String path) {
        return new InputStreamResource(storageService.getObjectByLabelAndBucketName(path, VIDEO_BUCKET));
    }


    public void saveHLSFilesToS3(File hlsDir, String s3Key) {
        File[] hlsFiles = hlsDir.listFiles();
        if (hlsFiles == null) return;

        for (File file : hlsFiles) {
            if (!file.isDirectory()) {
                storageService.saveObject(file, VIDEO_BUCKET, s3Key + "/" + file.getName());
                continue;
            }
            for (File file1 : file.listFiles()) {
                storageService.saveObject(file1, VIDEO_BUCKET, s3Key + "/" + file.getName() + "/" + file1.getName());
            }
        }
    }

    public Flux<MovieProjectionDto> getLibrary(Pageable pageable) {
        MovieFilter movieFilter = new MovieFilter();
        return Mono.fromCallable(() -> movieRepository.findBy(
                        filterDslMapper.createFilter(movieFilter),
                        q -> q.as(MovieProjection.class).page(pageable)
                ))
                .flatMapMany(page -> Flux.fromIterable(page.getContent())
                        .map(MovieProjectionDto::new));
    }
}
