package videoservice.service;

import lombok.RequiredArgsConstructor;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import videoservice.converter.MovieConverter;
import videoservice.dto.MovieLoading;
import videoservice.entity.Movie;
import videoservice.entity.MovieLoadHistory;
import videoservice.entity.enums.MovieStatusEnum;
import videoservice.ffmpeg.HlsConverter;
import videoservice.repository.MovieLoadingHistoryRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class LoadVideoService {
    private final VideoService videoService;
    private final MovieLoadingHistoryRepository movieLoadingHistoryRepository;
    private final MovieConverter movieConverter;
    private final HlsConverter hlsConverter;

    public Mono<MovieLoading> parseMovieDescriptionAndPoster(Mono<String> movieLoadingMono, Flux<FilePart> posterParts) {
        return movieLoadingMono
                .zipWith(posterParts.single())
                .flatMap(tuple -> {
                    String movieDescription = tuple.getT1();
                    FilePart posterPart = tuple.getT2();
                    MovieLoading movieLoading = new Gson().fromJson(movieDescription, MovieLoading.class);

                    // Читаем данные постера в массив байтов
                    return posterPart.content()
                            .map(this::extractBytes)
                            .reduce(this::concatenateArrays)
                            .doOnNext(movieLoading::setPoster)
                            .thenReturn(movieLoading);
                });
    }

    public Mono<Void> processAndUpload(File videoFile, String s3KeyPrefix, MovieLoadHistory newLoading) {
        String tempDir = System.getProperty("java.io.tmpdir");
        UUID movieId = UUID.randomUUID();
        log.info("Запись в хранилище S3");
        // Создаем уникальную временную папку для текущего видео

        Path movieTempDir = Path.of(tempDir, "video_" + movieId);
        File hlsDir = movieTempDir.toFile();
        hlsDir.mkdirs();
        Path output360pDir = movieTempDir.resolve("output_360p");
        Path output720pDir = movieTempDir.resolve("output_720p");
        Path output1080pDir = movieTempDir.resolve("output_1080p");
        try {
            Files.createDirectories(output360pDir);
            Files.createDirectories(output720pDir);
            Files.createDirectories(output1080pDir);
        } catch (IOException e) {
            newLoading.setLoadingStatus(MovieStatusEnum.ERROR);
            newLoading.setMessage(e.getMessage());
            throw new RuntimeException("Ошибка при создании директорий для HLS", e);
        }
        return Mono.fromRunnable(() -> {
            boolean isSuccess = true;
            try {
                hlsConverter.convertToHLS(videoFile.getAbsolutePath(),
                        output360pDir.toString(),
                        output720pDir.toString(),
                        output1080pDir.toString(),
                        movieTempDir.toString());
                videoService.saveHLSFilesToS3(hlsDir, s3KeyPrefix);
            } catch (IOException e) {
                isSuccess = false;
                newLoading.setMessage(e.getMessage());
                newLoading.setLoadingStatus(MovieStatusEnum.ERROR);
                throw new RuntimeException("Ошибка при обработке видео", e);
            } finally {
                if (isSuccess) {
                    newLoading.setLoadingStatus(MovieStatusEnum.FINISH);
                }
                movieLoadingHistoryRepository.save(newLoading);
                deleteDirectory(hlsDir);
            }
        }).subscribeOn(Schedulers.boundedElastic()).then();
    }

    private byte[] extractBytes(DataBuffer dataBuffer) {
        byte[] bytes = new byte[dataBuffer.readableByteCount()];
        dataBuffer.read(bytes);
        DataBufferUtils.release(dataBuffer);
        return bytes;
    }

    private byte[] concatenateArrays(byte[] array1, byte[] array2) {
        byte[] result = new byte[array1.length + array2.length];
        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        return result;
    }

    public Mono<Void> processMovieUpload(Flux<FilePart> movieParts, MovieLoading movieLoading) {
        MovieLoadHistory newLoading = startDownloadingVideo(movieLoading);
        UUID movieId = newLoading.getMovie().getId();
        Path tempFilePath = Path.of(System.getProperty("java.io.tmpdir"), movieId + "_movie.mp4");
        log.info("Запись файла в временные файлы");
        return DataBufferUtils.write(movieParts.flatMap(FilePart::content), tempFilePath)
                .then(Mono.defer(() -> processAndUploadMovie(tempFilePath, movieId.toString(), newLoading)))
                .doFinally(signalType -> deleteTemporaryFile(tempFilePath));

    }

    private Mono<Void> processAndUploadMovie(Path tempFilePath, String s3KeyPrefix, MovieLoadHistory newLoading) {
        File tempFile = tempFilePath.toFile();
        return processAndUpload(tempFile, s3KeyPrefix, newLoading);
    }

    private void deleteTemporaryFile(Path tempFilePath) {
        try {
            Files.deleteIfExists(tempFilePath);
        } catch (IOException e) {
            log.warn("Не удалось удалить временный файл: {}", tempFilePath, e);
        }
    }

    private void deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        directoryToBeDeleted.delete();
    }

    public MovieLoadHistory startDownloadingVideo(MovieLoading movieLoading) {
        log.info("Началась загрузка фильма {}", movieLoading.getTitle());
        Movie movie = movieConverter.convertRequestVideoToEntity(movieLoading);
        MovieLoadHistory newLoading = new MovieLoadHistory();
        newLoading.setMovie(movie);
        movieLoadingHistoryRepository.saveAndFlush(newLoading);
        return newLoading;
    }
}
