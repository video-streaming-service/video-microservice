package videoservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import videoservice.service.LoadVideoService;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("api/upload")
public class UploadVideoController {
    private final LoadVideoService loadVideoService;

    @PostMapping(value = "movies", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Mono<ResponseEntity<String>> uploadNewFilm(
            @RequestPart("movie_description") Mono<String> movieLoadingMono,
            @RequestPart("movie") Flux<FilePart> movieParts,
            @RequestPart("movie_poster") Flux<FilePart> posterParts) {
        return loadVideoService.parseMovieDescriptionAndPoster(movieLoadingMono, posterParts)
                .flatMap(movieLoading -> loadVideoService.processMovieUpload(movieParts, movieLoading))
                .thenReturn(ResponseEntity.ok("Фильм успешно загружен и обработан"));
    }
}
