package videoservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import videoservice.service.VideoService;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/movie/getContent")
public class CinemaController {
    private final VideoService videoStorageService;

    @GetMapping("/{videoId}/master.m3u8")
    public ResponseEntity<Resource> getMasterPlaylist(@PathVariable String videoId) {
        String filePath = "/" + videoId + "/master.m3u8";
        return serveFile(filePath);
    }

    @GetMapping("/{videoId}/{quality}/output.m3u8")
    public ResponseEntity<Resource> getQualityPlaylist(
            @PathVariable String videoId,
            @PathVariable String quality) {
        String filePath = "/" + videoId + "/" + quality + "/output.m3u8";
        return serveFile(filePath);
    }

    @GetMapping("/{videoId}/{quality}/{segment}.ts")
    public ResponseEntity<Resource> getSegment(
            @PathVariable String videoId,
            @PathVariable String quality,
            @PathVariable String segment) {
        String filePath = "/" + videoId + "/" + quality + "/" + segment + ".ts";
        return serveFile(filePath);
    }

    private ResponseEntity<Resource> serveFile(String filePath) {
        return ResponseEntity.ok(videoStorageService.getResource(filePath));
    }
}
