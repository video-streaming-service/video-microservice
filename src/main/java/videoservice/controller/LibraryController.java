package videoservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import videoservice.dto.MovieProjectionDto;
import videoservice.entity.Movie;
import videoservice.filter.MovieFilter;
import videoservice.projection.MovieProjection;
import videoservice.service.VideoService;

import java.util.List;


@RestController
@RequestMapping("/api/library")
@RequiredArgsConstructor
public class LibraryController {
    private final VideoService videoService;

    @GetMapping
    public Flux<MovieProjectionDto> getAllMovies(
                                                    @RequestParam(name = "page", defaultValue = "0") int page,
                                                    @RequestParam(name = "size", defaultValue = "20") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return videoService.getLibrary(pageable);
    }
}

