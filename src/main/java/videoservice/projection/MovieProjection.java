package videoservice.projection;

import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;
import java.util.UUID;

public interface MovieProjection {
    UUID getId();

    byte[] getPoster();

    String getDescription();

    String getOriginalName();

    String getGenre();


    default String getBase64Poster() {
        byte[] posterBytes = getPoster();
        if (posterBytes != null) {
            return Base64.getEncoder().encodeToString(posterBytes);
        }
        return null;
    }
}
