CREATE TABLE IF NOT EXISTS movie
(
    id            uuid primary key,
    description   text,
    original_name text not null,
    genre         text,
    poster        bytea
);

CREATE TABLE IF NOT EXISTS movie_load_history
(
    id             SERIAL primary key,
    creation_date  timestamp,
    movie_id       uuid references movie (id),
    loading_status text,
    message    text
);
